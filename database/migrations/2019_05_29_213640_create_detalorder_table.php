<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetalorderTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detalorder', function (Blueprint $table) {
            $table->bigIncrements('id_detalorder');
			$table->string('price_per_car')->nullable();//precio por auto
			$table->string('quantity')->nullable();//cantidad
			$table->string('order_date')->nullable();//fecha de orden
            $table->string('return_date')->nullable();// fecha de devolucion
			$table->bigInteger('days')->nullable(); //dias
			$table->bigInteger('namber_orders')->nullable();//numero de ordenes
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detalorder');
    }
}
