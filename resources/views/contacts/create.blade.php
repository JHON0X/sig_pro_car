@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Client </h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
    <form method="post" action="{{ route('client.store') }}">
          @csrf
          <div class="form-group">
              <label for="first_name">CI:</label>
              <input type="text" class="form-control" name="CI"/>
          </div>
          
          <div class="form-group">
              <label for="first_name">First Name:</label>
              <input type="text" class="form-control" name="name"/>
          </div>

          <div class="form-group">
              <label for="last_name">Last Name:</label>
              <input type="text" class="form-control" name="lastname"/>
          </div>
          <div class="form-group">
              <label for="first_name">address:</label>
              <input type="text" class="form-control" name="address"/>
          </div>
          <div class="form-group">
              <label for="first_name">city:</label>
              <input type="text" class="form-control" name="city"/>
          </div>
          <div class="form-group">
              <label for="first_name">mail:</label>
              <input type="text" class="form-control" name="mail"/>
          </div>
          <div class="form-group">
              <label for="first_name">phone:</label>
              <input type="text" class="form-control" name="phone"/>
          </div>

          <button type="submit" class="btn btn-primary-outline">Add Client</button>
      </form>
  </div>
</div>
</div>
@endsection
