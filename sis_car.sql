-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-07-2019 a las 22:51:46
-- Versión del servidor: 10.1.38-MariaDB
-- Versión de PHP: 7.3.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `sis_car`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `acount`
--

CREATE TABLE `acount` (
  `id_acount` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `acount`
--

INSERT INTO `acount` (`id_acount`, `name`, `password`, `created_at`, `updated_at`) VALUES
(1, 'jejej', '2525', '2019-06-04 21:19:45', '2019-06-04 21:22:31'),
(3, 'jaja', '2522', '2019-06-04 21:39:57', '2019-06-04 21:39:57');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `client`
--

CREATE TABLE `client` (
  `id_client` bigint(20) UNSIGNED NOT NULL,
  `CI` bigint(20) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `client`
--

INSERT INTO `client` (`id_client`, `CI`, `name`, `lastname`, `address`, `city`, `mail`, `phone`, `created_at`, `updated_at`) VALUES
(1, 21445, 'pepito', 'jaja', 'calle sucre', 'cbba', 'jajsjs@mail.com', 7485, '2019-06-05 04:17:29', '2019-06-05 00:41:27'),
(2, 21445, 'ieiei', 'baila', 'calle junin', 'cbba', 'asjda@mail.com', 7485, '2019-06-05 04:17:34', '2019-06-05 04:17:34'),
(3, 21445, 'iiqisa', 'nosabe', 'lejos', 'sa', 'kasl@mail.com', 7485, '2019-06-05 04:17:34', '2019-06-05 04:27:17'),
(4, 21445, 'serca', 'de', 'ahi', 'esta', 'elemail@mail.com', 7485, '2019-06-05 04:17:36', '2019-06-05 04:17:36'),
(5, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 19:47:44', '2019-07-15 19:47:44'),
(6, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 19:47:49', '2019-07-15 19:47:49'),
(7, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 19:49:53', '2019-07-15 19:49:53'),
(8, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 19:49:59', '2019-07-15 19:49:59'),
(9, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 19:50:07', '2019-07-15 19:50:07'),
(10, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 19:50:45', '2019-07-15 19:50:45'),
(11, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 19:54:19', '2019-07-15 19:54:19'),
(12, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 22:10:33', '2019-07-15 22:10:33'),
(13, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 22:10:39', '2019-07-15 22:10:39'),
(14, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-07-15 22:10:47', '2019-07-15 22:10:47');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cost`
--

CREATE TABLE `cost` (
  `id_cost` bigint(20) UNSIGNED NOT NULL,
  `client` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_cost` bigint(20) DEFAULT NULL,
  `discount` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `cost`
--

INSERT INTO `cost` (`id_cost`, `client`, `date`, `total_cost`, `discount`, `created_at`, `updated_at`) VALUES
(1, 'lolo', 'gg', 254, 25, '2019-06-05 00:41:57', '2019-06-05 00:41:57'),
(2, 'fff', 'asd', 254, 25, '2019-06-05 00:42:01', '2019-06-05 00:42:01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `detalorder`
--

CREATE TABLE `detalorder` (
  `id_detalorder` bigint(20) UNSIGNED NOT NULL,
  `price_per_car` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `quantity` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `return_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `days` bigint(20) DEFAULT NULL,
  `namber_orders` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `detalorder`
--

INSERT INTO `detalorder` (`id_detalorder`, `price_per_car`, `quantity`, `order_date`, `return_date`, `days`, `namber_orders`, `created_at`, `updated_at`) VALUES
(1, '255', 'blabla', 'sdasda', 'asfasd1', 25, 25, '2019-06-05 00:42:06', '2019-06-05 00:42:06'),
(2, '255', 'ced', 'sdasjejejda', 'kekek', 25, 25, '2019-06-05 00:42:09', '2019-06-05 00:42:09');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_05_29_212459_create_client_table', 1),
(4, '2019_05_29_213609_create_order_table', 1),
(5, '2019_05_29_213640_create_detalorder_table', 1),
(6, '2019_05_29_213702_create_cost_table', 1),
(7, '2019_05_29_213726_create_vehicle_table', 1),
(8, '2019_05_29_213740_create_model_table', 1),
(9, '2019_05_29_213815_create_user_table', 1),
(10, '2019_05_29_213844_create_acount_table', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `model`
--

CREATE TABLE `model` (
  `id_model` bigint(20) UNSIGNED NOT NULL,
  `name_model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `namber_of_seats` bigint(20) DEFAULT NULL,
  `manufacturing_date` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `model`
--

INSERT INTO `model` (`id_model`, `name_model`, `namber_of_seats`, `manufacturing_date`, `created_at`, `updated_at`) VALUES
(1, 'toyota', 98, 2000, '2019-06-05 00:41:49', '2019-07-03 22:14:22'),
(2, 'nissan', 98, 2000, '2019-06-05 00:41:50', '2019-06-05 00:41:50');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `order`
--

CREATE TABLE `order` (
  `id_order` bigint(20) UNSIGNED NOT NULL,
  `name_client` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `total_price` bigint(20) DEFAULT NULL,
  `detail_order` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `order`
--

INSERT INTO `order` (`id_order`, `name_client`, `total_price`, `detail_order`, `user`, `created_at`, `updated_at`) VALUES
(1, 'pepito', 2586, 'kaskd', 'fasd', '2019-06-05 00:42:15', '2019-06-05 00:42:15'),
(2, 'agapito', 2586, 'gsagdgas', 'dsadasd', '2019-06-05 00:42:18', '2019-06-05 00:42:18');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `password_resets`
--

INSERT INTO `password_resets` (`email`, `token`, `created_at`) VALUES
('jhonn@gmail.com', '$2y$10$q7W7YLcDcUoIeT9q5Xat/eWp/5B4AQgR4v0m.b/JrNVbWnuOexcgC', '2019-07-15 04:13:13');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `user`
--

CREATE TABLE `user` (
  `id_user` bigint(20) UNSIGNED NOT NULL,
  `login` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `acount` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `user`
--

INSERT INTO `user` (`id_user`, `login`, `password`, `acount`, `created_at`, `updated_at`) VALUES
(1, 'jhonn', 'jhonn', 'jhonn@gmail.com', '2019-06-05 00:42:26', '2019-06-05 00:42:26'),
(2, 'pepito', 'pepito1', 'pepe@gmail.com', '2019-06-05 00:42:29', '2019-06-05 00:42:29');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'jhonn', 'jhonn@gmail.com', NULL, '$2y$10$6xTBZiYKB0HOpZWbhVGQdelBUBqW2iEH57tZEh69B.wx7rTWWi0bC', NULL, '2019-07-04 06:35:15', '2019-07-04 06:35:15'),
(2, 'pepito', 'pepito@gmail.com', NULL, '$2y$10$9GK7kxDz.CEMLTR79Zi5YOBJP6Tq/cQ8ZwydJIv8VhFFWxSSjQN3m', NULL, '2019-07-04 06:37:22', '2019-07-04 06:37:22'),
(3, 'jhonn', 'jhonns@hotmail.com', NULL, '$2y$10$JU0pv36aZfdq8WnxkYcNve2vKbmLLyOYcucmO.ylomq0lfCF05h2.', NULL, '2019-07-15 04:16:31', '2019-07-15 04:16:31'),
(4, 'agapito', 'agapito@gmail.com', NULL, '$2y$10$u4nmAmeRR89RUCg4vUxBO.r7jIIq3HzmedCKyQhwAPHfJ51fUN55m', NULL, '2019-07-15 19:01:39', '2019-07-15 19:01:39');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `vehicle`
--

CREATE TABLE `vehicle` (
  `id_vehicle` bigint(20) UNSIGNED NOT NULL,
  `enrollent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `colour` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `brand` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mileage` bigint(20) DEFAULT NULL,
  `year` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `vehicle`
--

INSERT INTO `vehicle` (`id_vehicle`, `enrollent`, `model`, `colour`, `brand`, `mileage`, `year`, `created_at`, `updated_at`) VALUES
(1, 'ghjk', 'fafa', 'fasfg', 'frwfrwf', 55, 548, '2019-06-05 00:42:37', '2019-06-05 00:42:37'),
(2, 'fsdf', 'fsdf', 'f3rf', 'frwfrwf', 55, 548, '2019-06-05 00:42:40', '2019-06-05 00:42:40');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `acount`
--
ALTER TABLE `acount`
  ADD PRIMARY KEY (`id_acount`);

--
-- Indices de la tabla `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`id_client`);

--
-- Indices de la tabla `cost`
--
ALTER TABLE `cost`
  ADD PRIMARY KEY (`id_cost`);

--
-- Indices de la tabla `detalorder`
--
ALTER TABLE `detalorder`
  ADD PRIMARY KEY (`id_detalorder`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `model`
--
ALTER TABLE `model`
  ADD PRIMARY KEY (`id_model`);

--
-- Indices de la tabla `order`
--
ALTER TABLE `order`
  ADD PRIMARY KEY (`id_order`);

--
-- Indices de la tabla `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id_user`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `vehicle`
--
ALTER TABLE `vehicle`
  ADD PRIMARY KEY (`id_vehicle`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `acount`
--
ALTER TABLE `acount`
  MODIFY `id_acount` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `client`
--
ALTER TABLE `client`
  MODIFY `id_client` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT de la tabla `cost`
--
ALTER TABLE `cost`
  MODIFY `id_cost` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `detalorder`
--
ALTER TABLE `detalorder`
  MODIFY `id_detalorder` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `model`
--
ALTER TABLE `model`
  MODIFY `id_model` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `order`
--
ALTER TABLE `order`
  MODIFY `id_order` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `user`
--
ALTER TABLE `user`
  MODIFY `id_user` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `vehicle`
--
ALTER TABLE `vehicle`
  MODIFY `id_vehicle` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
