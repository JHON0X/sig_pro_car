<?php

namespace App\Http\Controllers;
use App\Model\client;
use Illuminate\Http\Request;

class clientcontroller extends Controller
{
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    public function index()
    {
        return view('contacts.create');
    }
    public function store(Request $request)
    {/*
        $request->validate([
            'CI'=>'CI',
            'mail'=>'mail'
        ]);*/

        $client = new client([
            'CI' => $request->get('CI'),
            'name' => $request->get('name'),
            'lastname' => $request->get('lastname'),
            'address' => $request->get('address'),
            'city' => $request->get('city'),
            'mail' => $request->get('mail'),
            'phone' => $request->get('phone')
        ]);
        $client->save();
        return redirect('/contacts')->with('success', 'Contact saved!');
    }
}
