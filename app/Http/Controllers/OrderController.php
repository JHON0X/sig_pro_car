<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\Order;
class OrderController extends Controller
{
    public function index(){
        return Order::all();
    }
    public function store(Request $request){
        $client = new Order();
        $client->fill($request->toArray())->save();
		return $client;
    }
    public function destroy($id){
        $client=Order::find($id);
        $client->delete();
    }
    public function show($id){
        $client=Order::find($id);
        return $client;
    }
    public function update($id,Request $request){
        $client=$this->show($id);
        $client->fill($request->all())->save();
        return $client;
    }
  
}
