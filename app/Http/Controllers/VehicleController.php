<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\model\Vehicle;
class VehicleController extends Controller
{
    public function index(){
        return Vehicle::all();
    }
    public function store(Request $request){
        $client = new Vehicle();
        $client->fill($request->toArray())->save();
		return $client;
    
    }
    public function destroy($id){
        $client=Vehicle::find($id);
        $client->delete();
    }
    public function show($id){
        $client=Vehicle::find($id);
        return $client;
    }
    public function update($id,Request $request){
        $client=$this->show($id);
        $client->fill($request->all())->save();
        return $client;
    }
   
}
