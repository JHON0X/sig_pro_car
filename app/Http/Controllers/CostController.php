<?php

namespace App\Http\Controllers;
use App\model\Cost;
use Illuminate\Http\Request;

class CostController extends Controller
{
    public function index(){
        return Cost::all();
    }
    public function store(Request $request){
        $client = new Cost();
        $client->fill($request->toArray())->save();
		return $client;
    }
    public function destroy($id){
        $client=Cost::find($id);
        $client->delete();
    }
    public function show($id){
        $client=Cost::find($id);
        return $client;
    }
    public function update($id,Request $request){
        $client=$this->show($id);
        $client->fill($request->all())->save();
        return $client;
    }
}
