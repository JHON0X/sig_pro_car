<?php

namespace App\Http\Middleware;

use Closure;

class models
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if((int)$request->header('models')===123456){
            return$next($request);
        }
        return response('Error',403);
    }
}
