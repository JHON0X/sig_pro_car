<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class Users extends Model
{
    protected $table='user';
    protected $primaryKey="id_user";
    public $timestamps=true;
    const created_at = 'create_ad';
    const update_at = 'update_ad';
    protected $fillable =[
        'login',
        'password',
        'acount',
        'created_ad',
        'updated_ad'
    ];
}
