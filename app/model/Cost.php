<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class Cost extends Model
{
    protected $table='cost';
    protected $primaryKey="id_cost";
    public $timestamps=true;
    const created_at = 'create_ad';
    const update_at = 'update_ad';
    protected $fillable =[
        'client',
        'date',
        'total_cost',
        'discount',
        'created_ad',
        'updated_ad'
    ];
}
