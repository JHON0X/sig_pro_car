<?php

namespace app\model;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table='order';
    protected $primaryKey="id_order";
    public $timestamps=true;
    const created_at = 'create_ad';
    const update_at = 'update_ad';
    protected $fillable =[
        'name_client',
        'total_price',
        'detail_order',
		'user',
        'created_ad',
        'updated_ad'
    ];
}
